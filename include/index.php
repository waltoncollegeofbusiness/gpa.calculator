<?php
$path_web = dirname($_SERVER['SCRIPT_NAME']);
?>

<form name='gpaform' onsubmit='return false'>

  <input type='hidden' value='0' name='Hours'></input>
  <input type='hidden' value='0' name='Points'></input>

  <div id='CumulativeData' style='display:none'>
    <div class='form-group'>
      <label for='CumulativeCredits'>Enter Current Completed Credit Hours: <small>(Not including transfer)</small></label>
      <input name='CumulativeCredits' class='form-control' onchange='calculateCumulative()' type='number'></input>
    </div>

    <div class='form-group'>
      <label for='CumulativeGPA'>Enter Current Cumulative GPA:</label>
      <input name='CumulativeGPA' class='form-control' onchange='calculateCumulative()' type='number' step='0.001'></input>
    </div>
    <div class='form-group'>
      <button class='btn btn-success'>Update</button>
    </div>
  </div>
  <div class='form-group'>
    <button data-btnshow='true' onclick='toggleCumulativeData(this)' class='btn btn-default'><i class='fa fa-plus'> Add Current Cumulative GPA</i></button>
  </div>

  <hr/><h3>Add Credits</h3>

  <p><label class='label label-primary'><a id='show_course_name'><i class="fa fa-plus"> Add Class Name (optional)</i></a></label></p>
  <div id='ClassName' class='form-group' style='display:none'>
    <label for='ClassName'>Class Name:</label>
    <input name='ClassName' class='form-control' type='text'></input>
  </div>

  <div class='form-group'>
    <label for='Grade'>Grade Received:</label>
    <select id='grade_selector' name='Grade' class='form-control'>
      <option value='4'>A (4)</option>
      <option value='3.67'>A- (3.67)</option>
      <option value='3.33'>B+ (3.33)</option>
      <option value='3'>B (3)</option>
      <option value='2.67'>B- (2.67)</option>
      <option value='2.33'>C+ (2.33)</option>
      <option value='2'>C (2)</option>
      <option value='1.67'>C- (1.67)</option>
      <option value='1.33'>D+ (1.33)</option>
      <option value='1'>D (1)</option>
      <option value='0.67'>D- (0.67)</option>
      <option value='0'>F (0)</option>
    </select>
  </div>

  <div class='form-group'>
    <label for='Credits'>Number of Credit Hours:</label>
    <input id='Credits' name='Credits' class='form-control' type='number'></input>
  </div>

  <div class='form-group'>
    <button name='B3' onclick='addRow()' class='btn btn-success'>Add</button>
  </div>

  <p id='addedCourses' name='addedCourses'></p>
  <div class='table-responsive' style='background-color:white'>
    <table id='tableAddedCourses' style='margin-bottom: 0px; display:none;' class='table table-hover table-striped table-bordered table-condensed'>
      <thead><tr><th>Class Name</th><th>Grade</th><th>Hours</th><th/></tr></thead>
      <tbody><tr><!-- this is a break row --></tr></tbody>
    </table>
  </div>

  <hr/><h3>Calculated GPA</h3>

  <div class='form-group'>
    <label for='TotalCredits'>Total Credits:</label>
    <input name='TotalCredits' class='form-control' type='number' value='0' disabled></input>
  </div>

  <div class='form-group'>
    <label for='GPA'>Calculated GPA:</label>
    <input name='GPA' class='form-control' type='number' value='0.000' step='0.001' disabled></input>
  </div>

  <div class='form-group'>
    <label for='CumulativeTotalCredits'>Cumulative Total Credits:</label>
    <input name='CumulativeTotalCredits' class='form-control' type='number' value='0' disabled></input>
  </div>

  <div class='form-group'>
    <label for='NewCumulativeGPA'>Cumulative Calculated GPA:</label>
    <input name='NewCumulativeGPA' class='form-control' type='number' value='0.000' step='0.001' disabled></input>
  </div>

  <div class='form-group'>
    <button name='B2' onclick='clearGPA()' class='btn btn-warning'>Reset</button>
  </div>

  <small>
    <p>The GPA Calculator is for estimation purposes only.</p>
    <p>The GPA as calculated does not in any way represent your official GPA.</p>
  </small>

</form>
<link rel="stylesheet" type="text/css" href="<?=$path_web?>/css/gpa.css">
<script>
$.getScript( "<?=$path_web?>/dist/js/all.min.js" );
$.getScript( "https://code.jquery.com/ui/1.11.4/jquery-ui.min.js" );
</script>
