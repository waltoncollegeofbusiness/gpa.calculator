function show_course_name(elem){
   $("#ClassName").show("slide").find("input").focus();
   $(elem).closest("p").hide("slide");
}

function toggleCumulativeData(btnshow){
  
  if($(btnshow).data("btnshow")){
    $("#CumulativeData").show("blind").find("input").first().focus();
    $(btnshow).data("btnshow",false).removeClass("btn-default").addClass("btn-danger").find("i").removeClass("fa-plus").addClass("fa-times").text(" Remove Current Cumulative GPA");
  } else {
    $("#CumulativeData").hide("blind").find("input").each(function(){
      $(this).val("");
    });
    $(btnshow).data("btnshow",true).removeClass("btn-danger").addClass("btn-default").find("i").removeClass("fa-times").addClass("fa-plus").text(" Add Current Cumulative GPA");
    calculateCumulative();
  }
}

function calculateGPA() {

  if(!$("#Credits").val()) return false;
  
  // update the displayed totals
  document.gpaform.TotalCredits.value = document.gpaform.Hours.value;
  document.gpaform.GPA.value = round(document.gpaform.Points.value / document.gpaform.Hours.value);

  // now call the calculateCumulative function to update
  calculateCumulative();

}

function calculateCumulative() {

  // calculate the cumulative GPA only if real numbers are entered by the user
  var CumulativeCredits = 0;
  var CumulativeGPA = 0;
  var TotalCredits = parseFloat(document.gpaform.TotalCredits.value);
  var CumulativeTotalCredits = TotalCredits;
  if (!isNaN(parseFloat(document.gpaform.CumulativeCredits.value))){
    CumulativeCredits = parseFloat(document.gpaform.CumulativeCredits.value);
    CumulativeTotalCredits += CumulativeCredits;
  }
  if (!isNaN(parseFloat(document.gpaform.CumulativeGPA.value)))
    CumulativeGPA = document.gpaform.CumulativeGPA.value;
  var tempCumulativePoints = parseFloat(CumulativeCredits) * parseFloat(CumulativeGPA);
  document.gpaform.NewCumulativeGPA.value = round((tempCumulativePoints + parseFloat(document.gpaform.Points.value)) / (parseFloat(CumulativeCredits) + parseFloat(TotalCredits)));
  document.gpaform.CumulativeTotalCredits.value = CumulativeTotalCredits;

}

function clearGPA() {

  // reset the form, since using a reset button does not remove options from a select object
  document.gpaform.TotalCredits.value = '0';
  document.gpaform.GPA.value = '0.000';
  document.gpaform.Hours.value = '0';
  document.gpaform.Points.value = '0';
  document.gpaform.Grade.selectedIndex = 0;
  document.gpaform.Credits.value = '';
  document.gpaform.CumulativeGPA.value = '';
  document.gpaform.CumulativeCredits.value = '';
  document.gpaform.CumulativeTotalCredits.value = 0;
  document.gpaform.NewCumulativeGPA.value = '0.000';

  $("#tableAddedCourses").hide("blind", function(){
    $("#tableAddedCourses tbody tr").remove();
  });
}

function addRow(){
  if(!$("#Credits").val()) return false;

  $("#tableAddedCourses").show("blind");

  var grade_selector = $("#grade_selector").clone().removeAttr("id").removeAttr("name").attr("onchange","updateRow(this)");
  grade_selector.find('option[value="' + $("#grade_selector").val() + '"]').attr('selected', 'true');

  var newRow = $("<tr data-grade='"+document.gpaform.Grade.options[document.gpaform.Grade.selectedIndex].value+"' data-hours='"+document.gpaform.Credits.value+"'style='display:none'>" +
    "<td><input type='text' value='" + document.gpaform.ClassName.value + "' class='form-control'></input></td>" +
    "<td>" + $('<div>').append(grade_selector).html() + "</td>" +
    "<td><input type='number' value='"+ document.gpaform.Credits.value +"' onkeyup='updateRow(this)' onchange='updateRow(this)' class='form-control'></input></td>"+
    "<td><a href='javascript:void(0)' onclick='deleteRow($(this))' class='btn btn-danger'><i class='fa fa-times'> Remove</i></a></td></tr>"
  );
  $('#tableAddedCourses tbody').append(
    newRow
  );
  newRow.show("highlight");

  // update the hidden totals
  document.gpaform.Hours.value = parseFloat(document.gpaform.Hours.value) + parseFloat(document.gpaform.Credits.value);
  document.gpaform.Points.value = parseFloat(document.gpaform.Points.value) + (parseFloat(document.gpaform.Grade.options[document.gpaform.Grade.selectedIndex].value) * parseFloat(document.gpaform.Credits.value));

  calculateGPA();
}

function updateRow(item){
  var row = $(item).closest("tr");
  var hours_old = parseFloat(row.attr("data-hours"));
  var hours_new = parseFloat(row.find("input").val());
  var grade_old = parseFloat(row.attr("data-grade"));
  var grade_new = parseFloat(row.find("select").val());
  row.attr("data-hours",hours_new);
  row.attr("data-grade",grade_new);
  // update the hidden totals
  document.gpaform.Hours.value = parseFloat(document.gpaform.Hours.value) + parseFloat(hours_new - hours_old);
  document.gpaform.Points.value = parseFloat(document.gpaform.Points.value) + ((grade_new*hours_new) - (grade_old*hours_old));
  calculateGPA();
}

function deleteRow(button){

  var row = button.closest("tr");
  var hours = parseFloat(row.attr("data-hours"));
  var grade = parseFloat(row.attr("data-grade"));
  
  // update the hidden totals
  document.gpaform.Hours.value = parseFloat(document.gpaform.Hours.value) - hours;
  document.gpaform.Points.value = parseFloat(document.gpaform.Points.value) - (grade*hours);
  calculateGPA();

  row.hide("highlight", {color: 'red'}, function(){
    row.remove();
  });
  
}

function round (n) {

  // this will return a number with exactly 3 digits after the decimal point
  n = Math.round(n * 1000) / 1000;
  n = (n + 0.0001) + '';
  return n.substring(0, n.indexOf('.') + 4);

}

$("#show_course_name").click(function(){ show_course_name(this); });